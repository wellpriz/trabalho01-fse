#!/usr/bin/python3

import sys,os,signal
import time
from datetime import datetime
from threading import Thread

import curses
from curses.textpad import rectangle

import socket
import json


from sala import Sala
from inout import InOut


firstKey = curses.KEY_F1
keyboardPress = -1

alarmeAtivado = False
alarmeAcionado = False

alarmeIncendio = False
salas = []

inputsValidos = ['presenca', 'fumaca', 'janela', 'contagem', 'porta', 'dth22']
outputsValidos = ['lampada', 'projetor','ar-condicionado', 'alarme']


def pintarInterface(stdscr):
    global alarmeIncendio, salas, inputsValidos, outputsValidos, keyboardPress
    global alarmeAtivado, alarmeAcionado

    stdscr.clear()
    stdscr.refresh()
    stdscr.nodelay(True)

    curses.start_color()
    curses.init_pair(1, curses.COLOR_CYAN, curses.COLOR_WHITE)
    curses.init_pair(2, curses.COLOR_BLACK, curses.COLOR_WHITE) # cor da letra em preto fundo branco
    curses.init_pair(3, curses.COLOR_BLACK, curses.COLOR_WHITE)
    curses.init_pair(4, curses.COLOR_CYAN, curses.COLOR_WHITE)
    curses.init_pair(5, curses.COLOR_BLACK, curses.COLOR_GREEN)
    curses.init_pair(6, curses.COLOR_BLACK, curses.COLOR_RED)
    curses.init_pair(7, curses.COLOR_BLACK, curses.COLOR_YELLOW)
    curses.curs_set(0)

    while(keyboardPress != curses.ascii.ESC):

        stdscr.erase()
        height, width = stdscr.getmaxyx()

        title = f"Servidor Central -> {sys.argv[1]}:{sys.argv[2]}"[:width-1]
        title_x = int((width // 2) - (len(title) // 2) - len(title) % 2)
        stdscr.addstr(0, title_x, title, curses.color_pair(1))

        footer = "Pressione 'Esc' para sair | 'a' para ligar/desligar alarme"
        stdscr.attron(curses.color_pair(1))
        stdscr.addstr(height-1, 0, footer)
        stdscr.addstr(height-1, len(footer), " " * (width - len(footer) - 1))
        stdscr.attroff(curses.color_pair(1))

        mid_x = width // 2
        subbar_h = 10
        rectangle_h = height - subbar_h
        margin_x = 2
        line = 3

        stdscr.attron(curses.color_pair(4))
        rectangle(stdscr, 1, 0, rectangle_h, mid_x)
        rectangle(stdscr, 1, mid_x, rectangle_h, width-1)
        rectangle(stdscr, rectangle_h+1, 0, rectangle_h+subbar_h-2, mid_x)
        rectangle(stdscr, rectangle_h+1, mid_x, rectangle_h+subbar_h-2, width-1)
        stdscr.attroff(curses.color_pair(4))

        if(len(salas) >= 1):
            titleCol1 = f"{salas[0].getNome()}"
            salat = salas[0].getContagem().getValue()
            stdscr.addstr(2, margin_x, f"Temperatura: {salas[0].getTemperatura():.1f} Cº", curses.color_pair(2))
            stdscr.addstr(3, margin_x, f"Humidade: {salas[0].getHumidade():.1f} %", curses.color_pair(2))
            pintarIOInterface(stdscr, line+2, margin_x, salas[0].getInputs())
            pintarIOInterface(stdscr, line+4+len(salas[0].getOutputs()), margin_x, salas[0].getOutputs())
            stdscr.addstr(rectangle_h+2, margin_x, f"Pessoas no prédio: {salat}", curses.color_pair(2))
        else:
            titleCol1 = f"Sem conexão!"
        stdscr.addstr(1, margin_x, titleCol1,curses.color_pair(1))

        if(len(salas) >= 2):
            titleCol2 = f"{salas[1].getNome()}"
            sala1 = salas[1].getContagem().getValue()
            stdscr.addstr(2, mid_x+margin_x, f"Temperatura: {salas[1].getTemperatura():.1f} Cº", curses.color_pair(2))
            stdscr.addstr(3, mid_x+margin_x, f"Humidade: {salas[1].getHumidade():.1f} %", curses.color_pair(2))
            pintarIOInterface(stdscr, line+2, mid_x+margin_x, salas[1].getInputs())
            pintarIOInterface(stdscr, line+4+len(salas[1].getOutputs()), mid_x+margin_x, salas[1].getOutputs())
            stdscr.addstr(rectangle_h+3, margin_x, f"Pessoas no {salas[0].getNome()}: {salat-sala1}", curses.color_pair(2))
            stdscr.addstr(rectangle_h+4, margin_x, f"Pessoas no {salas[1].getNome()}: {sala1}", curses.color_pair(2))
        else:
            titleCol2 = f"Sem conexão!"
        stdscr.addstr(1, margin_x+mid_x, titleCol2,curses.color_pair(1))

        stdscr.addstr(rectangle_h+1, margin_x, "Contador de pessoas", curses.color_pair(1))
        stdscr.addstr(rectangle_h+1, margin_x+mid_x, "Alarmes", curses.color_pair(1))

        if(alarmeIncendio):
            stdscr.addstr(rectangle_h+2, margin_x+mid_x, f"Alarme de incêndio ativado!", curses.color_pair(6))
        else:
            stdscr.addstr(rectangle_h+2, margin_x+mid_x, f"Prédio em perfeita condições!", curses.color_pair(5))

        if(alarmeAtivado):
            if(alarmeAcionado):
                stdscr.addstr(rectangle_h+3, margin_x+mid_x, f"Alarme acionado!", curses.color_pair(6))
            else:
                stdscr.addstr(rectangle_h+3, margin_x+mid_x, f"Alarme ativado!", curses.color_pair(5))
        else:
            stdscr.addstr(rectangle_h+3, margin_x+mid_x, f"Alarme desativado!    ", curses.color_pair(5))
            stdscr.addstr(rectangle_h+5, margin_x+mid_x, f"Para ativar feche     ", curses.color_pair(1))
            stdscr.addstr(rectangle_h+6, margin_x+mid_x, f"todas portas e janelas", curses.color_pair(1))
            stdscr.addstr(rectangle_h+7, margin_x+mid_x, f"retire todas pessoas  ", curses.color_pair(1))

        stdscr.refresh()
        keyboardPress = stdscr.getch()

        time.sleep(0.09)


def pintarIOInterface(stdscr, lineStart: int, margin_x: int, ios: list):
    for tmp_in in ios:
        if(tmp_in.getKey() != None):
            press = f"F{tmp_in.getKey() % curses.KEY_F0}"
            stdscr.addstr(lineStart, margin_x, f"{press}", curses.color_pair(3))
        else:
            press = ''

        valueColor = curses.color_pair(5) if tmp_in.getValue() else curses.color_pair(6)
        stdscr.addstr(lineStart, margin_x+len(press), f"  ", valueColor)
        stdscr.addstr(lineStart, margin_x+len(press)+3, f"{tmp_in.getTag()}", curses.color_pair(2))
        lineStart += 1


def lerSocket(con):
    global alarmeAtivado, salas

    sala = None
    while True:
        msg = con.recv(1024)
        if not msg: break
        msgs = [tmp+'}' for tmp in msg.decode('utf8').split('}') if tmp]

        for tmp in msgs:
            json_object = json.loads(tmp)
            if(json_object['mode'] == 'create'):
                if(json_object['type'] == "nome"):
                   sala = Sala(json_object['tag'])
                   if(json_object['tag'] == 'Térreo'):
                        salas.insert(0, sala)
                   else:
                        salas.append(sala)
                else:
                    criarIO(json_object,sala)

            elif(json_object['mode'] == 'update'):
                atualizarInput(json_object,sala)

    con.close() 


def enviarSocket(con):
    global keyboardPress, salas, alarmeIncendio, alarmeAtivado, alarmeAcionado

    gpio = -1
    value = -1
    while True:
        if(alarmeIncendio):
          time.sleep(0.09)
        if(keyboardPress == -1):
            continue
        elif(keyboardPress == curses.ascii.ESC):
            break
        elif(keyboardPress == ord('a')): 
            if(not alarmeAtivado): # verifica se alarme foi ativado
                
                ativarAlarme()
                ligaSireneAlarme(con)
            else:
                
                alarmeAtivado = False
                alarmeAcionado = False
                desligaSireneAlarme(con)

        for tmp in salas:
            if(keyboardPress == -1): break
            for tmp_io in tmp.getOutputs():
                if(tmp_io.getKey() == keyboardPress):
                    gpio = tmp_io.getGpio()
                    value = not tmp_io.getValue()
                    if(value):
                        log = 'Ativado'
                    else:    
                        log = 'Desativado'
                    escreverLog(f"{log} {tmp_io.getTag()}")
                    tmp_io.setValue(value)
                    keyboardPress = -1
                    break  
        keyboardPress = -1

        if(gpio == -1 and value == -1): continue
        buff = {"gpio":gpio,"value":value}
        nsent = con.send(json.dumps(buff).encode('utf-8'))
        if not nsent: break
        time.sleep(0.5)


def criarIO(json_object,sala):
    global salas, alarmeIncendio, inputsValidos, outputsValidos, firstKey
    msgIo = InOut(json_object['type'], json_object['tag'], json_object['gpio'], json_object['value'])

    if(sala == None):
        pass

    elif json_object['type'] in inputsValidos:
        for i, tmp in enumerate(salas):
            if(tmp.getNome() ==sala.getNome()):
                if(json_object['type'] == 'contagem'):
                    salas[i].setContagem(msgIo)
                elif(json_object['type'] == 'fumaca'):
                    salas[i].addInput(msgIo)
                    if(json_object['value'] and not alarmeIncendio):
                        escreverLog("Acionado alarme de incêndio")
                    alarmeIncendio = json_object['value']
                else:
                    salas[i].addInput(msgIo)

    elif json_object['type'] in outputsValidos:
        for i, tmp in enumerate(salas):
            if(tmp.getNome() ==sala.getNome()):
                msgIo.setKey(firstKey)
                firstKey += 1
                salas[i].addOutput(msgIo)


def atualizarInput(json_object,sala):
    global salas, inputsValidos, alarmeIncendio
    if(sala == None):
        pass

    elif json_object['type'] in inputsValidos:
        for i, tmp in enumerate(salas):
            if(tmp.getNome() ==sala.getNome()):
                if(json_object['type'] == 'fumaca'):
                    alarmeIncendio = json_object['value']
                if(json_object['type'] == 'dth22'):
                    salas[i].setTemperatura(json_object['temperatura'])
                    salas[i].setHumidade(json_object['humidade'])
                elif(json_object['type'] == 'contagem'):
                    salas[i].getContagem().setValue(json_object['value'])
                else:
                    for j, in_tmp in enumerate(salas[i].getInputs()):
                        if(in_tmp.getGpio() == json_object['gpio']):
                            salas[i].getInputs()[j].setValue(json_object['value'])
                if(not alarmeIncendio):
                    for tmp_sala in salas:
                        for tmp_sala_in in tmp_sala.getInputs():
                            if(tmp_sala_in.getType() == 'fumaca'):
                                if(tmp_sala_in.getValue()):
                                    alarmeIncendio = True



def initSocket(enderecoHost: str, porta: int):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock.bind((enderecoHost, porta))
    sock.listen(1)

    while True:
        con, cliente = sock.accept()
        lerSocketThread = Thread(target=lerSocket, args=(con,))
        lerSocketThread.start()
        enviarSocketThread = Thread(target=enviarSocket, args=(con,))
        enviarSocketThread.start()


def ativaralarme(con):
    global salas, alarmeIncendio
    for i, tmp in enumerate(salas):
        for j, tmp_in in enumerate(tmp.getOutputs()):
            if(tmp_in.getType() == 'alarme'):
                if(not tmp_in.getValue()):
                    buff = {"gpio":tmp_in.getGpio(),"value":True}
                    con.send(json.dumps(buff).encode('utf-8'))
                    salas[i].getOutputs()[j].setValue(True)

def ligaSireneAlarme(con):
    global salas, alarmeIncendio
    for i, tmp in enumerate(salas):
        for j, tmp_in in enumerate(tmp.getOutputs()):
            if(tmp_in.getType() == 'alarme'):
                if(not tmp_in.getValue()):
                    buff = {"gpio":tmp_in.getGpio(),"value":True}
                    con.send(json.dumps(buff).encode('utf-8'))
                    salas[i].getOutputs()[j].setValue(True)


def desligaSireneAlarme(con):
    global salas, alarmeIncendio
    for i, tmp in enumerate(salas):
        for j, tmp_in in enumerate(tmp.getOutputs()):
            if(tmp_in.getType() == 'alarme'):
                if(not tmp_in.getValue()):
                    buff = {"gpio":tmp_in.getGpio(),"value":True}
                    con.send(json.dumps(buff).encode('utf-8'))
                    salas[i].getOutputs()[j].setValue(True)

def ativarAlarme():
    global salas, alarmeAtivado, alarmeAcionado
    alarmeAtivado = not alarmeAtivado
    if(alarmeAtivado):
        for tmp in salas:
            for tmp_in in tmp.getInputs():
                if(tmp_in.getValue()):
                    alarmeAtivado = False
                    break
    if(alarmeAtivado):
        alarmeThread = Thread(target=rotinaAlarme)
        alarmeThread.start()
    else:
        alarmeAcionado = False
      


def rotinaAlarme():
    global salas, alarmeAtivado, alarmeAcionado
    while alarmeAtivado and not alarmeAcionado:
        for tmp in salas:
            for tmp_in in tmp.getInputs():
                if(tmp_in.getValue()):
                    alarmeAcionado = True
                    escreverLog("Acionado alarme")
                    break
        if not alarmeAtivado: break
        time.sleep(0.5)


def escreverLog(text: str):
    log = open('log.csv', 'a')
    log.write(f"{datetime.now()}, {text}\n")
    log.close()


def sigint_handler(signal, frame):
    pass


if __name__ == "__main__":

    signal.signal(signal.SIGINT, sigint_handler)

    if(len(sys.argv) != 3):
        print(f"Use: {sys.argv[0]} <endereço ip> <porta>")
        print(f"Exemplo: {sys.argv[0]} 192.168.0.53 10055")
        os._exit(os.EX_OK)

    socketThread = Thread(target=initSocket, args=(str(sys.argv[1]),int(sys.argv[2]),))
    socketThread.start()

    interfaceThread = Thread(target= curses.wrapper, args=(pintarInterface,))
    interfaceThread.start()
    interfaceThread.join()

    os._exit(os.EX_OK)
