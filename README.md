# Trabalho 1 (2022-2)

Trabalho 1 da disciplina de Fundamentos de Sistemas Embarcados (2022/2)

## 1. Objetivos

Este trabalho tem por objetivo a criação de um sistema distribuído de automação predial para monitoramento e acionamento de sensores e dispositivos de um prédio com múltiplas salas. O sistema deve ser desenvolvido para funcionar em um conjunto de placas Raspberry Pi com um servidor central responsável pelo controle e interface com o usuário e servidores distribuídos para leitura e acionamento dos dispositivos. Dentre os dispositivos envolvidos estão o monitoramento de temperatura e umidade, sensores de presença, sensores de fumaça, sensores de contagem de pessoas, sensores de abertura e fechamento de portas e janelas, acionamento de lâmpadas, aparelhos de ar-condicionado, alarme e aspersores de água em caso de incêndio.

Esse dispositivos serão acessados por meio das dashboard referente a cada sala.


A Figura 1 mostra a arquitetura do sistema.

![Figura](/figuras/arquitetura_trabalho_1.png)

A Figura 2 mostra a Dashboard do sistema.

![Figura](/figuras/dashboard.png)



# Servidor central

Servidor central responsável pelo controle e interface com o usuário e servidores distribuídos para leitura e acionamento dos dispositivos. 

![Servidor central](figuras/central.png)

## Dependências

* python3 (versão 3.7.3 ou superior)

## Como executar



```bash
cd servidor_central
python3 app.py <ip> <porta>
```

**Obs:** ```<ip>``` e ```<porta>``` utilizado por padrão no arquivo de configuração do servidor distribuído é respectivamente **192.168.1.146** e ***10861***

## Funcionamento

Após executar o servidor central estará a espera dos dois servidores distribuídos conectarem ao ```<ip>``` ```<porta>``` escolhida.

quando um servidor distribuído se conectar, o usuário poderá:

* ligar/desligar os dispositivos utilizando a teclha disposta a esquerda nome do dispositivo.
* ligar/desligar o alarme de segurança teclando a tecla **'a'**.
* Para sair pressione **'Esc'**.
* Um linha de log e gerado no arquivo **[log.csv](/servidor_central/log.csv)** para cada ativar/desativar dos dispositivos pelo usuário, também gera uma linha para ocorrência de acionamento dos alarmes de incêndio e de segurança

**Obs:**

* Quadrado vermelho = desligado
* Quadrado verde = ligado
* Após sair do servidor central , todos os servidores distribuídos serão encerrados

# Servidor distribuído

![Servidor Distribuido](figuras/distribuido.png)

## Dependências

* gcc (versão utilizada nos testes: c11)
* g++ (versão utilizada nos testes: c++17)
* [wiringPi](http://wiringpi.com/)
* make

## Como executar

```bash
cd servidor_distribuido
make all
./bin <arquivo de configuração>
```

Obs: Para o parâmetro ```<arquivo de configuração>``` pode ser utilizado de acordo com a sala :
rasp 42 e 46
 **[configuracao_sala01.json](/servidor_distribuido/configuracao_sala_01.json)** 

rap43 e 44
**[configuracao_sala02.json](/dservidor_distribuido/configuracao_sala_02.json)**



## Funcionamento

Após executar o servidor distribuído estará a espera do servidor central para ser conectar.

## Video
[![Watch the video](figuras/video.png)](figuras/video.mp4)
